package com.example.clevertask5.models
import com.google.gson.annotations.SerializedName

data class BankomatAndInfoBox(
    @SerializedName("gps_x") override val gps_x: Double,
    @SerializedName("gps_y") override val gps_y: Double
):MoneyTaker
