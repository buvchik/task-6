package com.example.clevertask5.models

import com.google.gson.annotations.SerializedName

class Filial(@SerializedName("GPS_X") override val gps_x: Double,
             @SerializedName("GPS_Y") override val gps_y: Double):MoneyTaker