package com.example.clevertask5.data.repository

import com.example.clevertask5.data.services.AtmService
import com.example.clevertask5.models.BankomatAndInfoBox
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit

class AtmRepositoty{
    fun getItems(retrofit: Retrofit): Observable<List<BankomatAndInfoBox>>{
        val bankService = retrofit.create(AtmService::class.java)
        return bankService.getAtmList()
    }
}