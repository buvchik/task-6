package com.example.clevertask5.data.repository

import com.example.clevertask5.data.services.FilialService
import com.example.clevertask5.models.Filial
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit

class FilialRepository {
    fun getItems(retrofit: Retrofit): Observable<List<Filial>> {
        val bankService = retrofit.create(FilialService::class.java)
        return bankService.getFilialsInfoList()
    }

}