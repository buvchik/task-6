package com.example.clevertask5.data.repository

import com.example.clevertask5.data.services.InfoService
import com.example.clevertask5.models.BankomatAndInfoBox
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit

class InfoRepository {
    fun getItems(retrofit: Retrofit): Observable<List<BankomatAndInfoBox>> {
        val bankService = retrofit.create(InfoService::class.java)
        return bankService.getInfoBoxList()
    }
}