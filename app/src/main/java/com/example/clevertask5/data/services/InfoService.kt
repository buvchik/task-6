package com.example.clevertask5.data.services

import com.example.clevertask5.models.BankomatAndInfoBox
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

interface InfoService {
    @GET("infobox?city=Гомель")
    fun  getInfoBoxList(): Observable<List<BankomatAndInfoBox>>
}