package com.example.clevertask5.data.services

import com.example.clevertask5.models.Filial
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

interface FilialService {
    @GET("filials_info?city=Гомель")
    fun  getFilialsInfoList(): Observable<List<Filial>>
}