package com.example.clevertask5.presentation

import android.graphics.PointF
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.clevertask5.R
import com.example.clevertask5.data.repository.AtmRepositoty
import com.example.clevertask5.data.repository.FilialRepository
import com.example.clevertask5.data.repository.InfoRepository
import com.example.clevertask5.models.MoneyTaker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.math.pow
import kotlin.math.sqrt

class viewModel {

    fun getData(point: LatLng, url: String, mMap: GoogleMap) {
        val retrofit =
            Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create()).build()

        val atmObservable = AtmRepositoty().getItems(retrofit)
        val infoBoxObservable = InfoRepository().getItems(retrofit)
        val filialObservable = FilialRepository().getItems(retrofit)
        Observable.concat(atmObservable, infoBoxObservable, filialObservable)
            .flatMapIterable { list -> list }.toSortedList { p0, p1 ->
                val dist1 =
                    sqrt((point.latitude - p0.gps_x).pow(2.0) + (point.longitude - p0.gps_y).pow(2.0))
                val dist2 =
                    sqrt((point.latitude - p1.gps_x).pow(2.0) + (point.longitude - p1.gps_y).pow(2.0))
                dist1.compareTo(dist2)
            }.toObservable().flatMapIterable { list -> list }.take(11) // почему-то -1
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe { item ->
                mMap.addMarker(MarkerOptions().position(LatLng(item.gps_x, item.gps_y)))
            }

    }
}