package com.example.clevertask5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.clevertask5.data.repository.AtmRepositoty
import com.example.clevertask5.data.repository.FilialRepository
import com.example.clevertask5.data.repository.InfoRepository
import com.example.clevertask5.databinding.ActivityMainBinding
import com.example.clevertask5.models.MoneyTaker
import com.example.clevertask5.presentation.viewModel
import com.google.android.gms.common.api.internal.ActivityLifecycleObserver.of
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.math.pow
import kotlin.math.sqrt

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val point = LatLng(52.425163, 31.015039)
        val url = "https://belarusbank.by/api/"

        val icon = BitmapDescriptorFactory.fromResource(R.drawable.point)
        mMap.addMarker(MarkerOptions().position(point).title(getString(R.string.custom_point))).setIcon(icon)

        mMap.moveCamera(CameraUpdateFactory.newLatLng(point))
        mMap.setMinZoomPreference(10f)

        viewModel().getData(point,url,mMap)



    }

}